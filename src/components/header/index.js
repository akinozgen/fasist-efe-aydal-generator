import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="container-lrg">
          <div className="flex col-12 spread">
            <Link
              to="/"
              className="logo primary-color launchaco-builder-hoverable logo__black"
            >
              Faşist Efe Aydal Generator
            </Link>
            <a href="#main-menu" id="menu" className="menu-toggle">
              <b className="nav-link mr0 secondary-color">☰</b>
            </a>
            <nav id="main-menu" role="navigation" className="main-menu">
              <a href="#!" id="main-menu-close" className="menu-close">
                <span className="bold">✕</span>
              </a>
              <a
                href="https://bitbucket.org/akinozgen/fasist-efe-aydal-generator"
                className="nav-link secondary-color"
              >
                Kaynak Kodu
              </a>
              <a
                href="mailto:akinozgen17@outlook.com"
                className="nav-link secondary-color"
              >
                Benle İletiş
              </a>
            </nav>
          </div>
        </div>
      </header>
    );
  }
}
