import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Hero extends Component {
  render() {
    return (
      <section className="section">
        <div className="container mb40">
          <div className="col-12 text-center">
            <h1 className="heading-lrg primary-color launchaco-builder-hoverable">
              Ekmek Kuran Çarpsın Verilerinizi Toplamıyorum.{" "}
            </h1>
            <h2 className="subheading secondary-color mt20 launchaco-builder-hoverable">
              İnanmayanlar ve mevzudan çakanlar yukarıdaki bağlantıdan kaynak
              koduna erişebilr...
            </h2>
            <div className="mt40">
              <Link
                to="/fasistlemeler"
                className="button mobile-text-center mt10 launchaco-builder-hoverable mr10 mobile-text-center accent-bg button__white"
              >
                <span> Sizin Faşistlemeleriniz </span>
              </Link>
              <Link
                to="/create"
                className="button mobile-text-center mt10 launchaco-builder-hoverable mr10 mobile-text-center accent-bg button__white"
              >
                <span> Kullansana 😉 </span>
              </Link>
              <a
                href="mailto:akinozgen17@outlook.com"
                target="_blank"
                className="button mobile-text-center mt10 launchaco-builder-hoverable"
              >
                <span> Fikirlerini Yazsana 😘 </span>
              </a>
            </div>
          </div>
        </div>
        <div className="container-lrg">
          <div className="col-12">
            <div className="browser mt75 launchaco-builder-hoverable">
              <div className="mask">
                <img
                  src="./assets/13983a64-7d2e-4cac-8178-8f8330f95301.jpg"
                  alt="Screenshot of App in Browser"
                  className="mask-img"
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
