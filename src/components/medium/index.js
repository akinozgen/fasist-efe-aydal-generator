import React, { Component } from "react";
import "./index.css";

export default class Medium extends Component {
  render() {
    return (
      <section
        id="medium-1"
        class="section green-accent"
        sitemeta="[object Object]"
        activepage="Landing"
      >
        <div class="container mb75 text-center">
          <div class="col-12">
            <h3 class="heading primary-color launchaco-builder-hoverable">
              Diğer Çalışmalarım
            </h3>
            <p class="subheading secondary-color launchaco-builder-hoverable">
              Pek bir şey yok aslında. Sayfa uzun olsun diye koydum...
            </p>
          </div>
        </div>
        <div class="container">
          <div class="col-12 launchaco-builder-hoverable works">
            <a className="panel" href="http://yemekx.surge.sh" target="_blank">
              <img
                src="http://yemekx.surge.sh/static/favicon.png"
                alt="YemekX - Yemek Tavsiye Robotu"
              />
              <span>YemekX</span>
              <p>
                YemekSepeti verilerini kullanarak size seçtiğiniz bölgeden
                rastgele yemek öneren web uygulaması.
              </p>
            </a>

            <a className="panel" href="http://rytmp.surge.sh" target="_blank">
              <img
                src="https://www.codester.com/static/uploads/items/9133/icon.png"
                alt="RYTMP - YouTube MP3 Player"
              />
              <span>RYTMP</span>
              <p>
                YouTube MP3 Player. Playlist oluşturma, dışa aktarma, içe
                aktarma, kuyruklama ve gerçek zamanlı YouTube arama mevcut.
              </p>
            </a>
          </div>
        </div>
      </section>
    );
  }
}
