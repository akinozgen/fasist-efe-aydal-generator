import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Pricing extends Component {
  render() {
    return (
      <section
        id="pricing-3"
        class="section lightblue-white"
        sitemeta="[object Object]"
        activepage="Landing"
      >
        <div class="container text-center mb75">
          <div class="col-12 text-center">
            <h4 class="heading primary-color launchaco-builder-hoverable">
              Bedava Bedava Bedava
            </h4>
            <p class="subheading secondary-color mt20 launchaco-builder-hoverable">
              Kıyamete kadar bedava.
            </p>
          </div>
        </div>
        <div class="container flex center-vertical">
          <div class="col-6">
            <div class="card pad30 launchaco-builder-hoverable">
              <div class="mb20">
                <div class="text-center">
                  <b class="bold primary-color">Başlangıç</b>
                  <div class="mt10">
                    <b class="heading primary-color">0</b>
                    <span class="paragraph secondary-color" />
                  </div>
                </div>
                <ul class="mt20">
                  <li class="flex mb20">
                    <svg
                      width="22px"
                      height="22px"
                      viewBox="0 0 22 22"
                      version="1.1"
                      xmlns="http://www.w3.org/2000/svg"
                      class="svg-fill mr20 noshrink"
                    >
                      <g transform="translate(4.000000, 5.000000)">
                        <path d="M5.24961475,8.39956394 L2.16512063,5.35475362 C1.74038521,4.93548271 1.05017933,4.9352057 0.624646383,5.35526395 C0.199019838,5.77541456 0.198881924,6.45614266 0.624129379,6.8759191 L4.35212111,10.555948 C4.38658274,10.6034965 4.42544251,10.6488955 4.46870038,10.6915969 C4.70907746,10.9288814 5.03375662,11.0320952 5.3475228,11.0013023 C5.59592563,10.9812599 5.83876209,10.8774981 6.02880771,10.6898975 C6.06831079,10.6509027 6.10414872,10.6096632 6.13632157,10.5665961 L13.9850992,2.81879759 C14.4107939,2.39857976 14.410861,1.71746456 13.985328,1.29740632 C13.5597015,0.8772557 12.8697673,0.877449143 12.444108,1.29763217 L5.24961475,8.39956394 Z" />
                      </g>
                    </svg>
                    <span class="span secondary-color">
                      Kıyamete kadar bedava
                    </span>
                  </li>
                  <li class="flex mb20">
                    <svg
                      width="22px"
                      height="22px"
                      viewBox="0 0 22 22"
                      version="1.1"
                      xmlns="http://www.w3.org/2000/svg"
                      class="svg-fill mr20 noshrink"
                    >
                      <g transform="translate(4.000000, 5.000000)">
                        <path d="M5.24961475,8.39956394 L2.16512063,5.35475362 C1.74038521,4.93548271 1.05017933,4.9352057 0.624646383,5.35526395 C0.199019838,5.77541456 0.198881924,6.45614266 0.624129379,6.8759191 L4.35212111,10.555948 C4.38658274,10.6034965 4.42544251,10.6488955 4.46870038,10.6915969 C4.70907746,10.9288814 5.03375662,11.0320952 5.3475228,11.0013023 C5.59592563,10.9812599 5.83876209,10.8774981 6.02880771,10.6898975 C6.06831079,10.6509027 6.10414872,10.6096632 6.13632157,10.5665961 L13.9850992,2.81879759 C14.4107939,2.39857976 14.410861,1.71746456 13.985328,1.29740632 C13.5597015,0.8772557 12.8697673,0.877449143 12.444108,1.29763217 L5.24961475,8.39956394 Z" />
                      </g>
                    </svg>
                    <span class="span secondary-color">
                      Öldükten sonra ödemeye başlayın. Yalnızda 9.99₺
                    </span>
                  </li>
                  <li class="flex mb20">
                    <svg
                      width="22px"
                      height="22px"
                      viewBox="0 0 22 22"
                      version="1.1"
                      xmlns="http://www.w3.org/2000/svg"
                      class="svg-fill mr20 noshrink"
                    >
                      <g transform="translate(4.000000, 5.000000)">
                        <path d="M5.24961475,8.39956394 L2.16512063,5.35475362 C1.74038521,4.93548271 1.05017933,4.9352057 0.624646383,5.35526395 C0.199019838,5.77541456 0.198881924,6.45614266 0.624129379,6.8759191 L4.35212111,10.555948 C4.38658274,10.6034965 4.42544251,10.6488955 4.46870038,10.6915969 C4.70907746,10.9288814 5.03375662,11.0320952 5.3475228,11.0013023 C5.59592563,10.9812599 5.83876209,10.8774981 6.02880771,10.6898975 C6.06831079,10.6509027 6.10414872,10.6096632 6.13632157,10.5665961 L13.9850992,2.81879759 C14.4107939,2.39857976 14.410861,1.71746456 13.985328,1.29740632 C13.5597015,0.8772557 12.8697673,0.877449143 12.444108,1.29763217 L5.24961475,8.39956394 Z" />
                      </g>
                    </svg>
                    <span class="span secondary-color">
                      Özellikler bu kadar
                    </span>
                  </li>
                  <li class="flex mb20">
                    <svg
                      width="22px"
                      height="22px"
                      viewBox="0 0 22 22"
                      version="1.1"
                      xmlns="http://www.w3.org/2000/svg"
                      class="svg-fill mr20 noshrink"
                    >
                      <g transform="translate(4.000000, 5.000000)">
                        <path d="M5.24961475,8.39956394 L2.16512063,5.35475362 C1.74038521,4.93548271 1.05017933,4.9352057 0.624646383,5.35526395 C0.199019838,5.77541456 0.198881924,6.45614266 0.624129379,6.8759191 L4.35212111,10.555948 C4.38658274,10.6034965 4.42544251,10.6488955 4.46870038,10.6915969 C4.70907746,10.9288814 5.03375662,11.0320952 5.3475228,11.0013023 C5.59592563,10.9812599 5.83876209,10.8774981 6.02880771,10.6898975 C6.06831079,10.6509027 6.10414872,10.6096632 6.13632157,10.5665961 L13.9850992,2.81879759 C14.4107939,2.39857976 14.410861,1.71746456 13.985328,1.29740632 C13.5597015,0.8772557 12.8697673,0.877449143 12.444108,1.29763217 L5.24961475,8.39956394 Z" />
                      </g>
                    </svg>
                    <span class="span secondary-color">
                      Kredi kartı bilgisi istemiyorum
                    </span>
                  </li>
                </ul>
              </div>
              <Link
                to="/create"
                class="button mobile-text-center mt10 launchaco-builder-hoverable button__full mobile-text-center accent-bg primary-color"
              >
                <span> Hemen Kullanmaya Başlayın </span>
              </Link>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
