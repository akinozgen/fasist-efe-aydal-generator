import React, { Component } from "react";
import "./index.css";

export default class Qutoe extends Component {
  copy() {
    const input = document.createElement("input");
    input.setAttribute("value", `${this.props.cumle} @${this.props.imza}`);
    // input.style.width = 0;
    // input.style.height = 0;
    // input.style.position = "absolute";
    // input.style.zIndex = -1;
    // input.style.visibility = "hidden";
    document.body.appendChild(input);
    input.select();
    document.execCommand("copy");
    input.remove();
  }

  render() {
    return (
      <blockquote class="quote-box">
        <p class="quotation-mark">“</p>
        <p class="quote-text">{this.props.cumle}</p>
        <hr />
        <div class="blog-post-actions">
          <p class="blog-post-bottom pull-left">{this.props.imza}</p>
        </div>
        <a
          target="_blank"
          className="twitter-share-button"
          href={`https://twitter.com/intent/tweet?text=${
            this.props.cumle
          }.\n @${this.props.imza}. \n @fasistlesene \n https://goo.gl/cRtZdQ`}
        >
          T
        </a>
        <a className="copy" onClick={this.copy.bind(this)}>
          C
        </a>
      </blockquote>
    );
  }
}
