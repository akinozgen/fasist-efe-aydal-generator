import React, { Component } from "react";

export default class Team extends Component {
  render() {
    return (
      <section
        id="team-2"
        class="section green-black"
        sitemeta="[object Object]"
        activepage="Landing"
      >
        <div class="container text-center mb75">
          <h3 class="heading primary-color launchaco-builder-hoverable">
            Bu Saçmalığın Ardındaki Takım
          </h3>
        </div>
        <div class="container-sml flex text-center">
          <div class="col-12 launchaco-builder-hoverable flex">
            <div class="team-card flex flex-column center-vertical center-horizontal w100">
              <div
                class="user-image mb20"
                style={{
                  backgroundImage:
                    'url("https://cdn.launchaco.com/images/0e7fb150-c33a-4302-a694-89c19540cfdd.png")'
                }}
              />
              <div>
                <b class="bold primary-color">Akın Özgen</b>
                <p class="paragraph primary-color mt10">
                  Front-End Developer (Antalya)
                </p>
                <p class="paragraph secondary-color mt20">
                  8 yıllık kod yazıcısı 2 yıldır profesyonel yazılım
                  geliştiricisi. Yüksek okul mezunu, askerliği kısaltmak için
                  açık öğretim öğrencisi Şimdilik bu kadar
                </p>
              </div>
              <div class="mt30">
                <a
                  href="https://twitter.com/akinozgen17"
                  target="_blank"
                  class="socialicons accent-bg twitter"
                />
                <a
                  href="https://instagram.com/akinozgen17"
                  target="_blank"
                  class="socialicons accent-bg instagram"
                />
                <a
                  href="https://github.com/akinozgen"
                  target="_blank"
                  class="socialicons accent-bg github"
                />
                <a
                  href="https://medium.com/@akinozgen"
                  target="_blank"
                  class="socialicons accent-bg medium"
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
