import React from "react";
import ReactDOM from "react-dom";
import Router from "./router";
import firebase from "firebase";
import firebaseConfig from "./.firebaseconfig.json";
import sw from "./registerServiceWorker";

firebase.initializeApp(firebaseConfig);

firebase
  .auth()
  .signInAnonymously()
  .catch(error => {
    console.table(error);
  });

sw();

ReactDOM.render(<Router />, document.getElementById("root"));
