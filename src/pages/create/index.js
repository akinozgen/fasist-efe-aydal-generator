import React, { Component } from "react";
import Header from "../../components/header";
import Select from "react-select/lib/Creatable";
import firebase from "firebase";
import "./index.css";
import swal from "sweetalert2";

export default class CreatePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: ["", "", "", "", ""],
      data: {
        isimler: [],
        sifatlar: [],
        zamanEkleri: [],
        hedefKitleler: [],
        eylemler: []
      }
    };

    this.handleClick = this.handleClick.bind(this);
    this.created = this.created.bind(this);
    this.save = this.save.bind(this);
    this.clear = this.clear.bind(this);
    this.getData();
  }

  clear() {
    this.setState({
      selected: ["", "", "", "", ""]
    });
  }

  handleClick(args, index) {
    this.setState({
      selected: this.state.selected.map((x, i) => {
        if (i === index) return args;
        return x;
      })
    });
  }

  created(args, index) {
    const { data } = this.state;
    data[index].push(args);

    const ref = firebase.database().ref("/data/" + index);
    ref.push().set(args);

    this.setState({ data });
  }

  save() {
    swal({
      title: "Son bir adım.",
      text: "Bir imza alabilir miyim?",
      type: "question",
      input: "text"
    }).then(params => {
      if (params.value !== "") {
        const payload = {
          imza: params.value,
          cumle: this.state.selected.map(x => x.label).join(" ")
        };
        const ref = firebase.database().ref("/fasistler");
        ref
          .push()
          .set(payload)
          .then(_ => {
            swal({
              text: "Oldu",
              text: "Kaydettim canısı",
              type: "success"
            });
          });
      } else {
        swal({
          title: "Hata",
          text: "İmzasız eklemiyorum. kb.",
          type: "error",
          showCloseButton: true
        });
      }
    });
  }

  async getData() {
    // get data from firebase
    const ref = firebase.database().ref("/data");
    ref.on("value", snapshot => {
      if (!snapshot.exists()) {
        console.log("Hata", snapshot);
        return;
      }

      const jsonData = snapshot.toJSON();

      const dataState = {};

      Object.keys(jsonData).forEach(x => {
        dataState[x] = Object.values(jsonData[x]);
      });

      this.setState({ data: dataState });
    });
  }

  render() {
    return (
      <div class="custom-color-547e8392-5ec1-0cac-77db-7288118ca68a">
        <Header />
        <section className="section">
          <div className="container mb40">
            <div className="col-12 text-center">
              <h1 className="subheading-lrg primary-color launchaco-builder-hoverable">
                Aşağıdan seçiyorsun kardşm. İstediğin yoksa yazarak
                ekleyebilirsin.
              </h1>
            </div>
            <br />
            <br />
            <form method="post">
              <div className="col-12 text-center">
                <div className="primary-color launchaco-builder-hoverable">
                  <Select
                    isClearable
                    onCreateOption={_ => this.created(_, "sifatlar")}
                    onChange={_ => this.handleClick(_, 0)}
                    placeholder="Efe'ye Sıfatınız (mesela faşist)"
                    value={this.state.selected[0]}
                    styles={{ option: () => ({ color: "black" }) }}
                    formatCreateLabel={str => (
                      <span>{str} kelimesini ekle</span>
                    )}
                    options={this.state.data.sifatlar.map(x => ({
                      label: x,
                      value: x
                    }))}
                  />

                  <Select
                    isClearable
                    onCreateOption={_ => this.created(_, "isimler")}
                    onChange={_ => this.handleClick(_, 1)}
                    placeholder="İsim (Efe Aydal seçmeniz önemle rica olunur)"
                    value={this.state.selected[1]}
                    styles={{ option: () => ({ color: "black" }) }}
                    formatCreateLabel={str => (
                      <span>{str} kelimesini ekle</span>
                    )}
                    options={this.state.data.isimler.map(x => ({
                      label: x,
                      value: x
                    }))}
                  />

                  <Select
                    isClearable
                    onCreateOption={_ => this.created(_, "zamanEkleri")}
                    onChange={_ => this.handleClick(_, 2)}
                    placeholder="Zaman eki (zorunlu değil)"
                    value={this.state.selected[2]}
                    styles={{ option: () => ({ color: "black" }) }}
                    formatCreateLabel={str => (
                      <span>{str} kelimesini ekle</span>
                    )}
                    options={this.state.data.zamanEkleri.map(x => ({
                      label: x,
                      value: x
                    }))}
                  />

                  <Select
                    isClearable
                    onCreateOption={_ => this.created(_, "hedefKitleler")}
                    onChange={_ => this.handleClick(_, 3)}
                    placeholder="Hedef Kitle/ler"
                    value={this.state.selected[3]}
                    styles={{ option: () => ({ color: "black" }) }}
                    formatCreateLabel={str => (
                      <span>{str} kelimesini ekle</span>
                    )}
                    options={this.state.data.hedefKitleler.map(x => ({
                      label: x,
                      value: x
                    }))}
                  />

                  <Select
                    isClearable
                    onCreateOption={_ => this.created(_, "eylemler")}
                    onChange={_ => this.handleClick(_, 4)}
                    placeholder="Eylem"
                    value={this.state.selected[4]}
                    styles={{ option: () => ({ color: "black" }) }}
                    formatCreateLabel={str => (
                      <span>{str} kelimesini ekle</span>
                    )}
                    options={this.state.data.eylemler.map(x => ({
                      label: x,
                      value: x
                    }))}
                  />

                  <button
                    disabled={this.state.selected.join("") === ""}
                    class="button mobile-text-center mt10 launchaco-builder-hoverable mr10 mobile-text-center red-bg button__white"
                    type="button"
                    onClick={this.clear}
                  >
                    <span>❌ Vazgeçtim Sil</span>
                  </button>

                  <button
                    class="button mobile-text-center mt10 launchaco-builder-hoverable mr10 mobile-text-center accent-bg button__white"
                    type="button"
                    onClick={_ =>
                      alert(
                        this.state.selected
                          .map(x => (x.label ? x.label : x))
                          .join(" ")
                      )
                    }
                  >
                    <span>💯 Test Ediniz </span>
                  </button>

                  <button
                    disabled={
                      this.state.selected
                        .filter((x, y) => y !== 2)
                        .indexOf("") !== -1
                    }
                    class="button mobile-text-center mt10 launchaco-builder-hoverable mr10 mobile-text-center accent-bg button__white"
                    type="button"
                    onClick={this.save}
                  >
                    <span>💾 Gönder Kaydedelim </span>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </section>
      </div>
    );
  }
}
