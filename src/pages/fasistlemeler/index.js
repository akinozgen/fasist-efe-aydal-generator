import React, { Component } from "react";
import Header from "../../components/header";
import Qutoe from "../../components/quote";
import firebase from "firebase";
import swal from "sweetalert2";

export default class FasistlemelerPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fasistlemeler: []
    };

    this.getData = this.getData.bind(this);

    this.getData();
  }

  getData() {
    const ref = firebase.database().ref("fasistler");
    ref.on("value", snapshot => {
      if (!snapshot.exists()) {
        swal({
          title: "Hata",
          text: "Verileri alamadım. İletişime geçersen mutlu olurum.😘"
        });
        return;
      }

      const jsonData = snapshot.toJSON();

      this.setState({ fasistlemeler: Object.values(jsonData) });
    });
  }

  render() {
    return (
      <div class="custom-color-547e8392-5ec1-0cac-77db-7288118ca68a">
        <Header />
        <section className="section">
          <div
            className="container mb40"
            style={{ maxWidth: "780px !important" }}
          >
            <div className="col-12 text-center">
              <h1 className="subheading-lrg primary-color launchaco-builder-hoverable">
                Burada kendin ve başkalarının faşistlemelerini bulabilirsin.
              </h1>
            </div>
            <br />
            <br />
            <form method="post">
              <div className="col-12 text-center">
                <div className="primary-color launchaco-builder-hoverable quotes">
                  {this.state.fasistlemeler.map(fasistleme => (
                    <Qutoe {...fasistleme} />
                  ))}
                </div>
              </div>
            </form>
          </div>
        </section>
      </div>
    );
  }
}
