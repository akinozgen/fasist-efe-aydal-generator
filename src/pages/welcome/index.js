import React, { Component } from "react";

import Header from "../../components/header";
import Hero from "../../components/hero";
import Medium from "../../components/medium";
import Team from "../../components/team";
import Pricing from "../../components/pricing";

export default class WelcomePage extends Component {
  render() {
    return (
      <div>
        <div class="custom-color-547e8392-5ec1-0cac-77db-7288118ca68a">
          <Header />
          <Hero />
        </div>
        <Medium />
        <Team />
        <Pricing />
      </div>
    );
  }
}
