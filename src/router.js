import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Link } from "react-router-dom";
import WelcomePage from "./pages/welcome";
import CreatePage from "./pages/create";
import FasistlemelerPage from "./pages/fasistlemeler";

export default class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={WelcomePage} />
          <Route path="/create" component={CreatePage} />
          <Route path="/fasistlemeler" component={FasistlemelerPage} />
        </Switch>
      </BrowserRouter>
    );
  }
}
